const md5 = require("md5");
const { Op } = require("sequelize");
const db = require("../db/models");

class UserModel {
  getAllUser = async () => {
    return await db.user.findAll({
      include: [db.user_bio],
      attributes: { exclude: "password" },
    });
  };

  registerNewUser = async (userData) => {
    db.user.create({
      username: userData.username,
      email: userData.email,
      password: md5(userData.password),
    });
  };

  loginUser = async (username, password) => {
    const dataUser = await db.user.findOne({
      where: { username: username, password: md5(password) },
      attributes: { exclude: "password" },
      raw: true,
    });
    return dataUser;
  };

  isUserExist = async (userData) => {
    const existUser = await db.user.findOne({
      where: {
        [Op.or]: [{ username: userData.username }, { email: userData.email }],
      },
    });
    if (existUser) {
      return true;
    } else {
      return false;
    }
  };

  registerUserBio = async (userId, fullname, address, phoneNumber, age) => {
    return await db.user_bio.create({
      fullname,
      address,
      phoneNumber,
      age,
      user_id: userId,
    });
  };

  updateUserBio = async (userId, fullname, address, phoneNumber, age) => {
    return await db.user_bio.update(
      { fullname, address, phoneNumber, age },
      { where: { user_id: userId } }
    );
  };

  isUserBioExist = async (userId) => {
    const bioExist = await db.user_bio.findOne({
      where: { user_id: userId },
    });

    if (bioExist) {
      return true;
    } else {
      return false;
    }
  };
  // PR exclude password
  findUserBio = async (userId) => {
    return await db.user_bio.findOne({
      include: [db.user],
      where: { user_id: userId },
    });
  };

  findUser = async (userId) => {
    return await db.user.findOne({
      include: [db.user_bio],
      where: { id: userId },
      attributes: { exclude: "password" },
    });
  };

  inputHistory = (userGame, userId) => {
    db.user_history.create({
      user_id: userId,
      pilihan: userGame.pilihan,
      judgement: userGame.judgement,
    });
  };

  // getUserHistory = async (userId) => {
  //   return await db.user_history.findOne({
  //     include: [db.user],
  //     where: { user_id: userId },
  //   });
  // };

  getUserHistory = async (userId) => {
    return await db.user.findOne({
      include: [{ model: db.game_room, as: "player1" }],
      where: { id: userId },
    });
  };

  // ============================================== GAME ===================================================

  createRoom = (player1, token) => {
    db.game_room.create({
      nama_room: player1.namaRoom,
      player1_id: token,
      player1_choice: player1.playerOneChoice,
      status_room: "Available",
    });
  };

  joinRoom = (player2, token, id) => {
    const roomId = parseInt(id);
    db.game_room.update(
      {
        player2_choice: player2.playerTwoChoice,
        player2_id: token,
      },
      { where: { id: roomId } }
    );
  };

  getAllRoom = async () => {
    const allRoom = await db.game_room.findAll({
      include: [
        {
          model: db.user,
          as: "player1",
        },
        {
          model: db.user,
          as: "player2",
        },
      ],
    });
    return allRoom;
  };

  getSingleRoom = async (id) => {
    const roomId = parseInt(id);
    const singleRoom = await db.game_room.findOne({
      where: { id: roomId },

      include: [
        {
          model: db.user,
          as: "player1",
        },
        {
          model: db.user,
          as: "player2",
        },
      ],
      raw: true,
    });
    return singleRoom;
  };

  RoomResult = async (result, id) => {
    const roomId = parseInt(id);
    await db.game_room.update(
      {
        player1_result: result[0],
        player2_result: result[1],
        status_room: "Finish",
      },
      { where: { id: roomId } }
    );
  };

  // RoomResult = async (p1Id, p2Id, p1Result, p2Result, result, id) => {
  //   const roomId = parseInt(id);
  //   const gameRoomDb = await db.game_room.update(
  //     {
  //       player1_result: result[0],
  //       player2_result: result[1],
  //       status_room: "Finish",
  //     },
  //     { where: { id: roomId } }
  //   );

  //   const userHistoryDb1 = await db.user_history.create({
  //     pilihan: p1Result,
  //     user_id: p1Id,
  //     judgement: result[0],
  //   });

  //   const userHistoryDb2 = await db.user_history.create({
  //     pilihan: p2Result,
  //     user_id: p2Id,
  //     judgement: result[1],
  //   });
  //   return gameRoomDb, userHistoryDb1, userHistoryDb2;
  // };

  singleUserHistory = async (id) => {
    try {
      const roomList = await db.game_room.findAll({
        attributes: [
          "nama_room",
          "updatedAt",
          "player1_id",
          "player2_id",
          "player1_result",
          "player2_result",
        ],
        where: {
          [Op.or]: [{ player1_id: id }, { player2_id: id }],
          [Op.and]: [
            { player1_result: { [Op.ne]: null } },
            { player2_result: { [Op.ne]: null } },
          ],
        },
        raw: true,
      });

      return roomList.map((room) => ({
        player_id: room.player1_id === id ? room.player1_id : room.player2_id,
        hasilPlayer:
          room.player1_id === id ? room.player1_result : room.player2_result,
        roomName: room.nama_room,
        updatedAt: room.updatedAt,
      }));
    } catch (error) {
      throw new Error("Gagal menerima room detail : " + error.message);
    }
  };

  playerVsCom = async (
    nama_room,
    id,
    player1_choice,
    player1_result,
    player2_choice,
    player2_result
  ) => {
    try {
      return await db.game_room.create({
        nama_room,
        player1_id: id,
        player1_choice,
        player1_result,
        player2_choice,
        player2_result,
        status_room: "Finish",
      });
    } catch (error) {
      console.log(error);
    }
  };
}

module.exports = new UserModel();
