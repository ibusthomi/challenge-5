"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class user_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user_history.belongsTo(models.user, { foreignKey: "user_id" });
    }
  }
  user_history.init(
    {
      pilihan: DataTypes.STRING,
      judgement: DataTypes.STRING,
      user_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "user_history",
    }
  );
  return user_history;
};
