"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user.hasOne(models.user_bio, { foreignKey: "user_id" });
      user.hasMany(models.user_history, { foreignKey: "user_id" });
      user.hasMany(models.game_room, {
        foreignKey: "player1_id",
        as: "player1",
      });
      user.hasMany(models.game_room, {
        foreignKey: "player2_id",
        as: "player2",
      });
    }
  }
  user.init(
    {
      username: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "user",
    }
  );
  return user;
};
