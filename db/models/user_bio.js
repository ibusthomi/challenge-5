"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class user_bio extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user_bio.belongsTo(models.user, { foreignKey: "user_id" });
    }
  }
  user_bio.init(
    {
      fullname: DataTypes.STRING,
      address: DataTypes.STRING,
      phoneNumber: DataTypes.INTEGER,
      age: DataTypes.INTEGER,
      user_id: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "user_bio",
    }
  );
  return user_bio;
};
